# Calcula l’àrea d’un triangle rectangle:
#   Altura = 5 i base = 10, crea el codi per l’operació aritmètica. 

altura = 5
base = 10

# Para mejorarlo podriamos preguntar al usuario los numeros
#    base = int(input("Introduce la base--> "))
#    altura = int(input("Introduce la altura--> "))

print("La area es ->" , (altura * base) / 2)