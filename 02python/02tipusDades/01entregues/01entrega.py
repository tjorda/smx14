# Mira la següent frase:
# “Sólo trabajo y nada de juegos hacen de Juan un niño aburrido”.
# Guarda cada paraula en variables separades, després mostra la frase en
# una sola línia. 

palabra1 = "Sólo"
palabra2 = "trabajo"
palabra3 = "y"
palabra4 = "nada"
palabra5 = "de"
palabra6 = "juegos"
palabra7 = "hacen"
palabra8 = "de"
palabra9 = "Juan"
palabra10 = "un"
palabra11 = "niño"
palabra12 = "aburrido"

print(palabra1 , palabra2 , palabra3 , palabra4 , palabra5 , palabra6 , palabra7 , palabra8 , palabra9 , palabra10 , palabra11 , palabra12)