# Quin tipus de variable hem de definir perquè en l’exercici anterior el programa interpreti les variables com a text i no faci la suma. 
x = str(15)
y = str(10)

z = x + y
print(f"{x} + {y} = {z}")

# Puedes hprueguntar al usuario lo que quiere sumar con una simple modificacion.
# Esto se ahora modificando las variables "x" "y" de la siguiente manera:
#    x = str(input("Introduce un numero--> "))
#    y = str(input("Introduce otro numero--> "))