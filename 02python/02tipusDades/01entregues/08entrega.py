# Calcula l’àrea d’aquest triangle: 

# Els paràmetres son els següents:
#   a = 12
#   h = 8
#   b = 5
#   c = 4 

# Crea un programa amb Python per calcular l’àrea d’aquest triangle. 

a = 12
h = 8
b = 5
c = 4

area1 = (a * h) / 2
area2 = (b * c) / 2

print("Area 1:" , area1)
print("Area 2:" , area2)
print("La area total:" , area1 + area2)