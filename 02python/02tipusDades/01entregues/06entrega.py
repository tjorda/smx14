# Crea el programa “Canvi de Divisa” en el que l’usuari introdueix una quantitat en euros i s’obté l’equivalent en USD. 

import time

print("Indica el canvio de divisas:")
print("EUR -> USD = 1")
print("USD -> EUR = 2")

time.sleep(0.5)

print("Para cancelar inserte: 0")
divisas = int(input("--> "))

time.sleep(1)

while divisas != 0:

    # Cambio a euro
    if divisas == 1:
        euro = int(input("Introduce la cantidad de EUR--> "))
        print(round(euro * 1.06, 3) , "$")

    # Cambio a dolar
    elif divisas == 2:
        dolar = int(input("Introduce la cantidad de --> "))
        print(round((dolar * 0.94), 3) , "€")

    else:
        print("¡¡ERROR!!")
        time.sleep(0.5)
        print("Especifica un numero entre los indicados")
    
    print("Indica el canvio de divisas:")
    print("EUR -> USD = 1")
    print("USD -> EUR = 2")

    time.sleep(0.5)

    divisas = int(input("--> "))