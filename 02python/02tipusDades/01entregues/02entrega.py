# Crea la següent operació amb variables: 15 + 10 = 25 
x = 15
y = 10

z = x + y
print(f"{x} + {y} = {z}")

# Puedes hprueguntar al usuario lo que quiere sumar con una simple modificacion.
# Esto se ahora modificando las variables "x" "y" de la siguiente manera:
#    x = int(input("Introduce un numero--> "))
#    y = int(input("Introduce otro numero--> "))