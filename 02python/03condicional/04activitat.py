# Realitzar un programa que sol·liciti per teclat dos números, si el primer és major
# al segon informar la seva suma i diferència, en cas contrari informar el producte i la
# divisió del primer respecte al segon.

num1 = int(input("Ingrsa un numero--> "))
num2 = int(input("Ingrsa otro numero--> "))

if num1 > num2:
    print("La suma es" , num1 + num2 , "i la diferencia es" , num1 - num2)
elif num1 < num2:
    print("El producto es" , num1 * num2 , "i la division es" , num1 / num2)