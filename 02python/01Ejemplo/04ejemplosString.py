# Ejemplo de cadena con operador concatenar
a = "Tinc"
b = 10
c = "anys"
print(a+str(b)+c)
print(f"{a} {b} {c}*")
print(a , b , c , end="*")


# Ejemplo operador multiplicador
print()

a = "hola"
b = 5
print(a*5)
print(a*5, sep = " ")