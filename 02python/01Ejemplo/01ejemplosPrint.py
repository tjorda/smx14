#Print la palabra reservada para imprimir.

print("hola mundo")
print("hola, mundo")
print("hola", "mundo") #La función print acepta varios parámetros

print("hola" "mundo")#Varios parámetros sin separador

print ("José Vicente")

#print (José Vicente) #Esto es un error porque no es cadena

#print "José Vicente" #En python 2 funciona/ En python 3 no

print ("Jose Sierra")
print ("jose", "sierra")
print('JOSE SIERRA') #La función acepta parámetros en comillas simples

#separadores
print("jose","sierra",sep="",end="\t") #El separador es el espacio pero aquí no es ninguno
print("jose","sierra",sep="_",end="@")
print("mi edad es 20")

#Activitat
#Montar un print Fundamentos***Programación***en...Python usando dos funciones print
#parametro1 : Fundamentos
#parametro2 : Programación
#parametro3 : en
#parametro4 : Python

print("Fundamentos","Programación","en",sep="***", end="...")
print("Python")