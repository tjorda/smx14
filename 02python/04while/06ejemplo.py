x = 1
while x < 11:
    if x % 2 == 1:
        print(x)
    x += 1

print(" ")

n = 3
while n > 0:
    print(n + 1)
    n -= 1
else: print(n)