contador = 5
while contador != 0:
    print("Dentro del ciclo: " , contador)
    contador -= 1
print("Fuera del ciclo" , contador)

# Otra forma de hacer un buble n veces
contador = 5
while contador > 0:
    print("Dentro del ciclo: " , contador)
    contador -= 1
print("Fuera del ciclo" , contador)