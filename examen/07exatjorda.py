xurros_xoco = int(input("Quats xurros amb xocolata? "))
xurros = int(input("Quats xurros sense xocolata? "))
money = int(input("Quats diners entrega? "))

preu = 0
dif = 0

preu = xurros + (xurros_xoco * 2)
if preu > 10:
    preu = preu - 1

print(f"El preu total és {preu} euros.")
if preu == money:
    print("Ha entregat la cuantitat exacta.")
elif preu > money:
    dif = preu - money
    if dif == 1:
        print(f"Li falta {dif} euro")
    else:
        print(f"Li falten {dif} euros")
elif preu < money:
    dif = money - preu
    if dif == 1:
        print(f"El seu canvi és {dif} euro")
    else:
        print(f"El seu canvi és {dif} euros")