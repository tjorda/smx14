###################### EJEMPLOS PRINT ######################

a = input("Dame un numero --> ")  #intorduce un string
b = input("Dame otro numero --> ")   #intorduce un string
print(a + b)

c = int(input("Dame un numero -->"))   #intorduce un string y lo pasa a entero
d = int(input("Dame un numero -->"))   #intorduce un string y lo pasa a entero
print(c + d)

e = input("Dame un numero -->")   #intorduce un string
f = int(input("Dame un numero -->"))   #intorduce un string y lo pasa a entero

print(int(e) + f)
print(e + f)  #No se puede implementar

