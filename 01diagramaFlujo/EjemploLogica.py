################ EMPLOS DE BOOLEANOS ############

a = int(input("Introduce un valor --> "))
b = int(input("Introduce otro valor --> "))
c = int(input("Introduce un tercer valor --> "))

if a > b:
	print (f"a es mayor que b")
	print (a > b)
if b > c:
	print (f"b es mayor que c")
	print (b > c)
else:
	print (f"c es mayor que b")
	print (b > c)
if a > b and a > b:
	print (f"a es mayor que b y c")
	print (a > b and a > c)

############### VARIABLES LOGICAS ##############

import time
time.sleep(2)
print (" ")
print ("VARIABLES LOGICAS")
time.sleep(1)

logica1 = True
logica2 = False
logica3 = True

print (logica1 and logica2)
print (logica1 or logica2)
print (not logica1)
print (logica1 or logica2 or logica3)

