#VARIABLES T/F#
ft = False

#HORAS#
while ft == False:
    h = int(input("Introduce las horas--> "))
    if h < 24 and h >= 0:
        ft = True
    else:
        print("El valor debe ser un numero entero entre 0 y 23")
ft = False

#MINUTOS#
while ft == False:
    m = int(input("Introduce los minutos--> "))
    if m < 60 and m >= 0:
        ft = True
    else:
        print("El valor debe ser un numero entero entre 0 y 59")
ft = False

#SEGUNDOS#
while ft == False:
    s = int(input("Introduce los segundos--> "))
    if s < 60 and s >= 0:
        ft = True
    else:
        print("El valor debe ser un numero entero entre 0 y 59")
ft = False

#ACUMULADOR#
c = 0

c = h*3600 + m*60 + s
print(f"Han pasado {c} segundos")
